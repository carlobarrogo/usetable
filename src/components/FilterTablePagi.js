import React from "react";
import { useTable, useGlobalFilter, useAsyncDebounce, usePagination } from 'react-table'
import '../App.css';


// Define a default UI for filtering
function GlobalFilter({ globalFilter,setGlobalFilter,}) {
    
    const [value, setValue] = React.useState(globalFilter)
    const onChange = useAsyncDebounce(value => {
        setGlobalFilter(value || undefined)
    }, 200)

    return (
        <span className="searchfilter">
            Search:{' '}
            <input className="form-control" value={value || ""} onChange={e => { setValue(e.target.value); onChange(e.target.value);}}
                placeholder= "Filter Table"
            />
        </span>
    )
}


function Table({ columns, data }) {

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        page,
        prepareRow,
        state,
        preGlobalFilteredRows,
        setGlobalFilter,
        canPreviousPage,
        canNextPage,
        pageOptions,
        pageCount,
        gotoPage,
        nextPage,
        previousPage,
        setPageSize,
        state: { pageIndex, pageSize },
    } = useTable(
        {
            columns,
            data,
            initialState: { pageIndex: 0 },
        },
       
        useGlobalFilter,
        usePagination
    )

    return (
        <div>
            <GlobalFilter
                preGlobalFilteredRows={preGlobalFilteredRows}
                globalFilter={state.globalFilter}
                setGlobalFilter={setGlobalFilter}
            />
            <table className="table" {...getTableProps()}>
                <thead>
                    {headerGroups.map(headerGroup => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map(column => (
                                <th {...column.getHeaderProps()}>
                                    {column.render('Header')}
                                    {/* Render the columns filter UI */}
                                    
                                </th>
                            ))}
                        </tr>
                    ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                    {page.map((row, i) => {
                        prepareRow(row)
                        return (
                            <tr {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                    return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                })}
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <br />
            <div className="pagination">
        <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
          {'<<'}
        </button>{' '}
        <button onClick={() => previousPage()} disabled={!canPreviousPage}>
          {'<'}
        </button>{' '}
        <button onClick={() => nextPage()} disabled={!canNextPage}>
          {'>'}
        </button>{' '}
        <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}> 
          {'>>'}
        </button>{' '}
        <span className="pageofpage">
          Page{' '}
          <strong>
            {pageIndex + 1} of {pageOptions.length}
          </strong>{' '}
        </span>
        <span >
          || Go to page:{' '}
          <input
            type="number"
            defaultValue={pageIndex + 1}
            onChange={e => {
              const page = e.target.value ? Number(e.target.value) - 1 : 0
              gotoPage(page)
              
            }}
            className = "gotopage"
          
          />
        </span>{' '}
        <select
          value={pageSize}
          onChange={e => {
            setPageSize(Number(e.target.value))
          }}
          className ="showpage"
        >
          {[10, 20, 30, 40, 50].map(pageSize => (
            <option key={pageSize} value={pageSize}>
              Show {pageSize}
            </option>
          ))}
        </select>
      </div>

        </div>
    )
}



function FilterTable() {
    const columns = React.useMemo(
        () => [
            {
                Header: 'FullName',
                columns: [
                    {
                        Header: 'First Name',
                        accessor: 'firstName',
                    },
                    {
                        Header: 'Last Name',
                        accessor: 'lastName',
                    },
                ],
            },
            {
                Header: 'Details',
                columns: [
                    {
                        Header: 'Age',
                        accessor: 'age',
                    },
                    {
                        Header: 'Address',
                        accessor: 'address',
                    },
                    {
                        Header: 'Gender',
                        accessor: 'gender',
                    },
                    {
                        Header: 'Contact Number',
                        accessor: 'contact',
                    },
                ],
            },
        ],
        []
    )

    const data = [
        {
            "firstName": "Carlo",
            "lastName": "Barrogo",
            "age": 95,
            "address": "Lipa City",
            "contact": "09000000000",
            "gender": "M"
        },
        {
            "firstName": "Dan Mikko",
            "lastName": "Mazo",
            "age": 61,
            "address": "Tugeuegarao City",
            "contact": "09999999999",
            "gender": "M"
        },
        {
            "firstName": "Erik Julius",
            "lastName": "Castro",
            "age": 7,
            "address": "Makati City",
            "contact": "09888888888",
            "gender": "M"
        },
        {
            "firstName": "Ralph",
            "lastName": "Villa",
            "age": 16,
            "address": "Makati City",
            "contact": "09777777777",
            "gender": "M"
        },
        {
            "firstName": "Mico",
            "lastName": "Vital",
            "age": 73,
            "address": "Makati City",
            "contact": "09777777777",
            "gender": "M"
        },
        {
            "firstName": "Yuichi",
            "lastName": "Gonzales",
            "age": 39,
            "address": "Makati City",
            "contact": "09666666666",
            "gender": "M"
        },
        {
            "firstName": "Dan Mikko",
            "lastName": "Mazo",
            "age": 61,
            "address": "Tugeuegarao City",
            "contact": "09999999999",
            "gender": "M"
        },
        {
            "firstName": "Dan Mikko",
            "lastName": "Mazo",
            "age": 61,
            "address": "Tugeuegarao City",
            "contact": "09999999999",
            "gender": "M"
        },
        {
            "firstName": "Dan Mikko",
            "lastName": "Mazo",
            "age": 61,
            "address": "Tugeuegarao City",
            "contact": "09999999999",
            "gender": "M"
        },
        {
            "firstName": "Dan Mikko",
            "lastName": "Mazo",
            "age": 61,
            "address": "Tugeuegarao City",
            "contact": "09999999999",
            "gender": "M"
        },
        {
            "firstName": "Dan Mikko",
            "lastName": "Mazo",
            "age": 61,
            "address": "Tugeuegarao City",
            "contact": "09999999999",
            "gender": "M"
        },
        {
            "firstName": "Dan Mikko",
            "lastName": "Mazo",
            "age": 61,
            "address": "Tugeuegarao City",
            "contact": "09999999999",
            "gender": "M"
        },
        {
            "firstName": "Dan Mikko",
            "lastName": "Mazo",
            "age": 61,
            "address": "Tugeuegarao City",
            "contact": "09999999999",
            "gender": "M"
        },
        {
            "firstName": "Dan Mikko",
            "lastName": "Mazo",
            "age": 61,
            "address": "Tugeuegarao City",
            "contact": "09999999999",
            "gender": "M"
        },
        {
            "firstName": "Dan Mikko",
            "lastName": "Mazo",
            "age": 61,
            "address": "Tugeuegarao City",
            "contact": "09999999999",
            "gender": "M"
        },
        {
            "firstName": "Dan Mikko",
            "lastName": "Mazo",
            "age": 61,
            "address": "Tugeuegarao City",
            "contact": "09999999999",
            "gender": "M"
        },
        
    ]

    return (
        <Table columns={columns} data={data} />
    )
}

export default FilterTable;