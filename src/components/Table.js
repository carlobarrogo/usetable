import React from "react";
import { useTable } from 'react-table';
import '../App.css';


function Table() {

    const columns = [
        {
            Header: 'FullName',
            columns: [
                {
                    Header: 'First Name',
                    accessor: 'firstName',
                },
                {
                    Header: 'Last Name',
                    accessor: 'lastName',
                },
            ],
        },
        {
            Header: 'Details',
            columns: [
                {
                    Header: 'Age',
                    accessor: 'age',
                },
                {
                    Header: 'Address',
                    accessor: 'address',
                },
                {
                    Header: 'Gender',
                    accessor: 'gender',
                },
                {
                    Header: 'Contact Number',
                    accessor: 'contact',
                },
            ],
        },
    ];

    const data = [
        {
            "firstName": "Carlo",
            "lastName": "Barrogo",
            "age": 95,
            "address": "Lipa City",
            "contact": "09000000000",
            "gender": "M"
        },
        {
            "firstName": "Dan Mikko",
            "lastName": "Mazo",
            "age": 61,
            "address": "Tugeuegarao City",
            "contact": "09999999999",
            "gender": "M"
        },
        {
            "firstName": "Erik Julius",
            "lastName": "Castro",
            "age": 7,
            "address": "Makati City",
            "contact": "09888888888",
            "gender": "M"
        },
        {
            "firstName": "Ralph",
            "lastName": "Villa",
            "age": 16,
            "address": "Makati City",
            "contact": "09777777777",
            "gender": "M"
        }
    ]


    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
    } = useTable({
        columns,
        data,
    })

    return (

        <table className="table" {...getTableProps()}>
            <thead>
                {headerGroups.map(headerGroup => (
                    <tr {...headerGroup.getHeaderGroupProps()}>
                        {headerGroup.headers.map(column => (
                            <th {...column.getHeaderProps()}>{column.render('Header')}</th>
                        ))}
                    </tr>
                ))}
            </thead>
            <tbody {...getTableBodyProps()}>
                {rows.map((row, i) => {
                    prepareRow(row)
                    return (
                        <tr {...row.getRowProps()}>
                            {row.cells.map(cell => {
                                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                            })}
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}

export default Table;