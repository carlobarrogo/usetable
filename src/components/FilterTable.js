import React from "react";
import { useTable, useFilters, useGlobalFilter, useAsyncDebounce } from 'react-table'
import '../App.css';


// Define a default UI for filtering
function GlobalFilter({ globalFilter,setGlobalFilter,}) {
    
    const [value, setValue] = React.useState(globalFilter)
    const onChange = useAsyncDebounce(value => {
        setGlobalFilter(value || undefined)
    }, 200)

    return (
        <span className="searchfilter">
            Search:{' '}
            <input className="form-control" value={value || ""} onChange={e => { setValue(e.target.value); onChange(e.target.value);}}
                placeholder= "Filter Table"
            />
        </span>
    )
}


function Table({ columns, data }) {

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
        state,
        preGlobalFilteredRows,
        setGlobalFilter,
    } = useTable(
        {
            columns,
            data,
         
        },
        useFilters,
        useGlobalFilter
    )

    return (
        <div>
            <GlobalFilter
                preGlobalFilteredRows={preGlobalFilteredRows}
                globalFilter={state.globalFilter}
                setGlobalFilter={setGlobalFilter}
            />
            <table className="table" {...getTableProps()}>
                <thead>
                    {headerGroups.map(headerGroup => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map(column => (
                                <th {...column.getHeaderProps()}>
                                    {column.render('Header')}
                                    {/* Render the columns filter UI */}
                                    
                                </th>
                            ))}
                        </tr>
                    ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                    {rows.map((row, i) => {
                        prepareRow(row)
                        return (
                            <tr {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                    return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                })}
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <br />

        </div>
    )
}



function FilterTable() {
    const columns = React.useMemo(
        () => [
            {
                Header: 'FullName',
                columns: [
                    {
                        Header: 'First Name',
                        accessor: 'firstName',
                    },
                    {
                        Header: 'Last Name',
                        accessor: 'lastName',
                    },
                ],
            },
            {
                Header: 'Details',
                columns: [
                    {
                        Header: 'Age',
                        accessor: 'age',
                    },
                    {
                        Header: 'Address',
                        accessor: 'address',
                    },
                    {
                        Header: 'Gender',
                        accessor: 'gender',
                    },
                    {
                        Header: 'Contact Number',
                        accessor: 'contact',
                    },
                ],
            },
        ],
        []
    )

    const data = [
        {
            "firstName": "Carlo",
            "lastName": "Barrogo",
            "age": 95,
            "address": "Lipa City",
            "contact": "09000000000",
            "gender": "M"
        },
        {
            "firstName": "Dan Mikko",
            "lastName": "Mazo",
            "age": 61,
            "address": "Tugeuegarao City",
            "contact": "09999999999",
            "gender": "M"
        },
        {
            "firstName": "Erik Julius",
            "lastName": "Castro",
            "age": 7,
            "address": "Makati City",
            "contact": "09888888888",
            "gender": "M"
        },
        {
            "firstName": "Ralph",
            "lastName": "Villa",
            "age": 16,
            "address": "Makati City",
            "contact": "09777777777",
            "gender": "M"
        }
    ]

    return (
        <Table columns={columns} data={data} />
    )
}

export default FilterTable;